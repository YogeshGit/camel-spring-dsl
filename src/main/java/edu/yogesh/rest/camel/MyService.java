package edu.yogesh.rest.camel;

import org.apache.camel.CamelContext;
import org.springframework.stereotype.Component;
@Component
public class MyService {
	CamelContext camelContext;

	public MyService(CamelContext context) {
		this.camelContext = context;
	}
	
	public void display() {
		this.camelContext.getComponentNames();
	}

}
