package edu.yogesh.rest.camel.route;


import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JaxbDataFormat;
import org.springframework.stereotype.Component;

import com.yogesh.rest.model.Note;

@Component
public class MyRouter extends RouteBuilder {

	@Override
	public void configure() throws Exception {
	JaxbDataFormat jaxbDataFormat = new JaxbDataFormat(); jaxbDataFormat.setContextPath(Note.class.getPackage().getName()); 
		restConfiguration().component("restlet").host("localhost").port("8080").contextPath("camal-spring");

		rest("/say").get("/hello").to("direct:hello").get("/bye").consumes("application/json").to("direct:bye")
				.post("/bye").to("mock:update");
		rest("/abc").post().consumes("application/json").to("bean:myBean?method=doSomething").produces("application/json");
		rest("/xyz").post().consumes("application/xml").to("bean:myBean?method=doSomethingWithXml").produces("application/xml");
		
		rest("/marshel").post().consumes("application/xml").to("direct:converXml").produces("application/xml");

		from("direct:hello").transform().constant("Hello World");
		from("direct:bye").to("bean:myBean?method=doSomething");
		from("direct:converXml").unmarshal(jaxbDataFormat).to("bean:myBean?method=doSomethingWithXmlMarsheller").marshal(jaxbDataFormat);
	};

}