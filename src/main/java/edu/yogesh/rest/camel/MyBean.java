package edu.yogesh.rest.camel;

import org.apache.camel.Body;

import org.apache.camel.Exchange;

import org.apache.camel.Message;

import org.springframework.stereotype.Component;

import com.yogesh.rest.model.Note;

@Component

public class MyBean {

	public String doSomething(Exchange exchange, Message message) {
		String responseJson = "{ \"name\":\"John\", \"age\":30, \"car\":\"BMW\" }";

		if (exchange != null) {

			String body = exchange.getIn().getBody(String.class);

			System.out.println("This is Body" + body);

		}

		System.out.println("This Method is called");
		return responseJson;

	}

	public String doSomethingWithXml(Exchange exchange, Message message) {
		String responseXml = "This XML file does not appear to have any style information associated with it. The document tree is shown below.\r\n"
				+ "<note>\r\n" + "<to>Tove</to>\r\n" + "<from>Jani</from>\r\n" + "<heading>Reminder</heading>\r\n"
				+ "<body>Don't forget me this weekend! TO MAKE Rout Program</body>\r\n" + "</note>";

		if (exchange != null) {

			String body = exchange.getIn().getBody(String.class);

			System.out.println("This is Body" + body);

		}

		System.out.println("This Method is called");
		return responseXml;

	}

	public Note doSomethingWithXmlMarsheller(Note note) {
		if (note != null) {
			System.out.println(note.getBody()+" Body");
		}
		Note responseNote = new Note();
		responseNote.setBody("If you are looking this means it is working");
		responseNote.setFrom("Yogesh");
		responseNote.setTo("ALL");

		return responseNote;

	}

}