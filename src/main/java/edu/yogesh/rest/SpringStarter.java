package edu.yogesh.rest;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import edu.yogesh.rest.camel.MyService;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class SpringStarter {

	private static final String CAMEL_SERVLET_NAME = "CamelServlet";
	private static final String CAMEL_URL_MAPPING = "/*";
	public MyService myService;

	public static void main(String[] args) {
		SpringApplication.run(SpringStarter.class, args);
		System.out.println("Wel Come to Spring world...!");

	}

}